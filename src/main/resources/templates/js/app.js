const React = require('react');
const ReactDOM = require('react-dom');
const client = require('./client');

class App extends React.Component{

    render(){
        return(
            <h2>Hello index</h2>
        )
    }

}

ReactDOM.render(
    <App />,
    document.getElementById('react')
)