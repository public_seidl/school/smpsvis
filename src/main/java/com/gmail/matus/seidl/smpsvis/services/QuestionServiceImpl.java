package com.gmail.matus.seidl.smpsvis.services;

import com.gmail.matus.seidl.smpsvis.model.Question;
import com.gmail.matus.seidl.smpsvis.repositories.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-02
 */
@Service
public class QuestionServiceImpl implements QuestionService {

    private QuestionRepository repository;

    @Autowired
    public QuestionServiceImpl(QuestionRepository repository) {
        this.repository = repository;
    }


    @Override
    public  List<Question> findAll() {
        List<Question> questions = new ArrayList<>();
        repository.findAll().iterator().forEachRemaining(questions::add);
        return questions;
    }
}
