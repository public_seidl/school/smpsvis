package com.gmail.matus.seidl.smpsvis.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-02
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "question")
    private List<QOption> qOptions = new ArrayList<>();


    private String result;

    @Enumerated(value = EnumType.STRING)
    private TypeOfQuestion typeOfQuestion;

    public Question(String name, String result, TypeOfQuestion typeOfQuestion) {
        this.name = name;
        this.result = result;
        this.typeOfQuestion = typeOfQuestion;
    }

    public void addOptions(QOption... options){
        Arrays.stream(options).forEach(option -> {
            option.setQuestion(Question.this);
            Question.this.qOptions.add(option);
        });
    }
}
