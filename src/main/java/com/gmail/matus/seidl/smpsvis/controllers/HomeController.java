package com.gmail.matus.seidl.smpsvis.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-02
 */
@Controller
public class HomeController {

    @RequestMapping(value = {"","/","index","/index"},method = RequestMethod.GET)
    public String index(){
        return "index";
    }
}
