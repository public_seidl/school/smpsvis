package com.gmail.matus.seidl.smpsvis.repositories;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-02
 */

import com.gmail.matus.seidl.smpsvis.model.Question;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface  QuestionRepository extends CrudRepository<Question,Long> {
}
