package com.gmail.matus.seidl.smpsvis.services;


import com.gmail.matus.seidl.smpsvis.model.Question;

import java.util.List;
import java.util.Optional;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-02
 */
public interface QuestionService {

    List<Question> findAll();
}
