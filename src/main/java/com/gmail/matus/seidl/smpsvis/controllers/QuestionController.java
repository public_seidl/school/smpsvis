package com.gmail.matus.seidl.smpsvis.controllers;

import com.gmail.matus.seidl.smpsvis.model.Question;
import com.gmail.matus.seidl.smpsvis.services.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-02
 */
@RestController
public class QuestionController {

    private QuestionService service;

    @Autowired
    public QuestionController(QuestionService service) {
        this.service = service;
    }


    @RequestMapping(value ="/all",method = RequestMethod.GET)
    public List<Question> getAllQuestions(){
        List<Question> tmp = service.findAll();
        return tmp;
    }
}
