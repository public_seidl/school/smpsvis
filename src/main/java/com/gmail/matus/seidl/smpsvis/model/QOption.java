package com.gmail.matus.seidl.smpsvis.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-02
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"question"})
@Entity
public class QOption {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(value = EnumType.STRING)
    private TypeOfQuestion type;
    private String msg;

    @ManyToOne
    @JsonBackReference
    private Question question;

    private boolean correct;

    public QOption(TypeOfQuestion type, String msg, Question question, boolean correct) {
        this.type = type;
        this.msg = msg;
        this.question = question;
        this.correct = correct;
    }
}
