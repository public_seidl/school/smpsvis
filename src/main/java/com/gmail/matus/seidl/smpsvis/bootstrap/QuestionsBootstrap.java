package com.gmail.matus.seidl.smpsvis.bootstrap;

import com.gmail.matus.seidl.smpsvis.model.QOption;
import com.gmail.matus.seidl.smpsvis.model.Question;
import com.gmail.matus.seidl.smpsvis.model.TypeOfQuestion;
import com.gmail.matus.seidl.smpsvis.repositories.QOptionRepository;
import com.gmail.matus.seidl.smpsvis.repositories.QuestionRepository;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-02
 */
@Component
public class QuestionsBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private QuestionRepository repository;
    private QOptionRepository optionRepository;

    @Autowired
    public QuestionsBootstrap(QuestionRepository repository, QOptionRepository optionRepository) {
        this.repository = repository;
        this.optionRepository = optionRepository;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
        repository.saveAll(getQuestions());
    }

    public List<Question> getQuestions() {
        List<Question> questions = new ArrayList<>();

        Question tmp = new Question("Čo je to secure coding?","Vyvijat kod tak, aby bol bezpecny a splnal bezpecnostne standarty", TypeOfQuestion.QUESTION_WITHOUT_OPTIONS);
        Question tmp1 = new Question("Co je to bezpectnostne riziko?","Je to miera hrozby, ktorej je system vystaveny a urovne zranitelnosti na konkretnu hrozbu", TypeOfQuestion.QUESTION_WITHOUT_OPTIONS);
        Question tmp2 = new Question("Ak najvyššiu pozíciu može človek vo firme zastávať?","CEO -  Chief Executive Officer", TypeOfQuestion.QUESTION_WITHOUT_OPTIONS);
        Question tmp3 = new Question("Vymenuj 4 kategórie(triedy) údajpv z hľadiska ochrany.","public, internal, confidential, strictly confidential", TypeOfQuestion.QUESTION_WITHOUT_OPTIONS);

        Question tmp4 = new Question("Ako mozeme rozdelit firemne spolocnosti?",null, TypeOfQuestion.QUESTION_WITH_OPTIONS);
        QOption qOptionA = new QOption(TypeOfQuestion.A,"spolocnosti zamerane na it",tmp4,true);
        QOption qOptionB = new QOption(TypeOfQuestion.B,"spolocnosti zamerane na financie a biznis",tmp4,true);
        QOption qOptionC = new QOption(TypeOfQuestion.C,"spolocnosti zamerane na socialne vztahy",tmp4,false);
        QOption qOptionD = new QOption(TypeOfQuestion.D,"spolocnosti zamerane na obchodne vztahy",tmp4,false);
        tmp4.addOptions(qOptionA,qOptionB,qOptionC,qOptionD);


        Question tmp5 = new Question("Definujte, čo je to princíp štyroch očí (four eyes principle) v oblasti bezpečnosti.","Princíp štyroch očí je požiadavka, aby dve osoby odsúhlasili nejakú činnosť pred tým, ako sa podnikne. Zjednodušuje rozdeľovanie práv a zvyšuje transparentnosť.", TypeOfQuestion.QUESTION_WITHOUT_OPTIONS);
        Question tmp6 = new Question("Otazka pochadza z riadenia informacnej bezpecnosti, konkretne sa tyka manazmentu riadenia rizik. Kazda \"vacsia firma\" by mala zastresovat manazment biznis kontinuity. Co riadi biznis kontinuita?","Spracuva mozne rizika, scenare hrozieb a vytvara postupy na rychlu obnovu a spatnu prevadzku biznisu.", TypeOfQuestion.QUESTION_WITHOUT_OPTIONS);
        Question tmp7 = new Question("Vymenuj druhy rizík","Podnikateľské, Strategické, Enviromentálne, Trhové, Finančné, Prevádzkové, Splnenie noriem", TypeOfQuestion.QUESTION_WITHOUT_OPTIONS);

        questions.add(tmp);
        questions.add(tmp1);
        questions.add(tmp2);
        questions.add(tmp3);
        questions.add(tmp4);
        questions.add(tmp5);
        questions.add(tmp6);
        questions.add(tmp7);
       // questions.add(tmp);
        return questions;
    }
}
