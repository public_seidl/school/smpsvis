package com.gmail.matus.seidl.smpsvis.model;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-02
 */
public enum TypeOfQuestion {
    QUESTION_WITH_OPTIONS,
    A,B,C,D,
    QUESTION_WITHOUT_OPTIONS
}
