package com.gmail.matus.seidl.smpsvis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmpsvisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmpsvisApplication.class, args);
	}
}
